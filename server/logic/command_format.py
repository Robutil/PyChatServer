from datetime import datetime
from threading import Thread
from time import sleep

import os
from gtts import gTTS

from server.models.channel import Channel


class CommandFormat(object):
    def __init__(self, session_info, channels):
        self.session_info = session_info
        self.channels = channels
        self.audio_path = 'client/audio/'

    def handle_command(self, msg):
        param = msg.split(' ')
        subject = param[0]
        param = param[1:]

        if subject == 'channel' or subject == 'c':
            return self.handle_channel_command(param)

        elif subject == 'help' or subject == 'h':
            return self.handle_create_help_info(param[0])

        elif subject == 'show' or subject == 's':
            return self.handle_show_command(param)

        elif subject == 'audio' or subject == 'a':
            return self.handle_audio_request(param)

        else:
            return None

    def handle_channel_command(self, param):
        subject = param[0]
        param = param[1:]

        if subject == 'default':
            return self.join_channel(self.channels[0])

        elif subject == 'join' or subject == 'j':
            return self.join_channel(param[0])

        elif subject == 'leave' or subject == 'l':
            return self.leave_channel(param[0])

        elif subject == 'show':
            return self.show_users_in_channel(param[0])

        else:
            return None

    def handle_create_help_info(self, channel_name):
        helper = 'Use the @ symbol to send private messages' \
                 '\tUsage: @[username] message\n' \
                 'Use the / symbol to send commands\n' \
                 '\tUsage: /channel\n' \
                 '\t\tjoin [channel_name] -> Join channel (doubles for create)\n' \
                 '\t\tleave [(optional) channel_name] -> leave (current) channel\n' \
                 '\tExample: /channel join my_channel\n' \
                 '\tUsage: /show\n' \
                 '\t\tshows active channels'
        return self.session_info.protocol.send_message(6, channel_name + ' ' + helper)

    def handle_show_command(self, param):
        channel_name = param[0]
        if False:
            # Create cases here in the future
            pass
        else:
            return self.handle_show_all_channels(channel_name)

    def handle_show_all_channels(self, channel_name):
        helper = ''
        for channel in self.channels:
            if len(helper) == 0:
                helper += 'global'  # todo: why does global name changes?
            else:
                helper += ', ' + channel.name

        return self.session_info.protocol.send_message(6, channel_name + ' Active channels: ' + helper)

    def join_channel(self, ch):
        ch = self.check_channel_exist(ch)

        ch.add_client(self.session_info)
        self.session_info.channels.append(ch)

        self.send_user_joined_channel_message(ch)

        helper = {'channel': ch.name}
        return self.session_info.protocol.send_message(1001, helper)

    def send_user_joined_channel_message(self, ch):
        username = self.session_info.username
        time = datetime.now().strftime('[%H:%M]')

        ch.announce(ch.name + " " + time + " User: " + username + " joined channel " + ch.name, 1005,
                    username)

    # TODO: merge this and above
    def send_user_left_channel_message(self, ch):
        username = self.session_info.username
        time = datetime.now().strftime('[%H:%M]')

        ch.announce(ch.name + " " + time + " User: " + username + " left channel " + ch.name, 1007,
                    username)

    def check_channel_exist(self, user_channel):
        if not isinstance(user_channel, Channel):
            for ch in self.channels:
                if ch.name == user_channel:
                    return ch

            new_channel = Channel(user_channel)
            self.channels.append(new_channel)
            return new_channel

        else:
            return user_channel

    def leave_channel(self, ch):
        ch = self.check_channel_exist(ch)

        # Cannot leave default channel
        if ch.name == self.channels[0].name:
            return None

        ch.remove_client(self.session_info)

        index = 0
        for user_channel in self.session_info.channels:
            if user_channel.name == ch.name:
                self.session_info.channels.pop(index)
            index += 1

        self.send_user_left_channel_message(ch)

        # If the last client leaves, remove channel completely
        if len(ch.get_client_names()) == 0:
            index = 0
            for temp in self.channels:
                if temp.name == ch.name:
                    self.channels.pop(index)
                index += 1

        helper = {'channel': ch.name}
        return self.session_info.protocol.send_message(1009, helper)

    def show_users_in_channel(self, channel_name):
        helper = {'users': None}

        if channel_name[0] is not '@':
            for ch in self.channels:
                if ch.name == channel_name:
                    users = ch.get_client_names()
                    helper = {'users': users, 'channel': channel_name}

        return self.session_info.protocol.send_message(1003, helper)

    def create_private_message(self, payload):
        message_target = payload[0]
        message = ' '.join(payload[1:])

        for target in self.session_info.communication_handler.client_list:
            if target.username == message_target:
                # Inform target of private message
                helper = {'channel': '@' + self.session_info.username}
                target.protocol.send_message(1011, helper)

                # Inform initiator of private message
                helper = {'channel': '@' + message_target}
                self.session_info.protocol.send_message(1011, helper)

                # Send the message TODO: get this from message class
                time = datetime.now().strftime('[%H:%M]')

                format_msg = '@' + self.session_info.username + ' ' + time + '\t' + self.session_info.username + ' : ' + message
                target.protocol.send_message(6, format_msg)

                format_msg = '@' + message_target + ' ' + time + '\t' + self.session_info.username + ' : ' + message
                return self.session_info.protocol.send_message(6, format_msg)

    def handle_audio_request(self, payload):
        if payload[0] == 'publish':
            payload[1] = 'audio/' + str(payload[1])
            helper = {'src': payload[1], 'channel': payload[3], 'persist': payload[2],
                      'origin:': self.session_info.username}

            channel = self.check_channel_exist(helper['channel'])
            channel.announce(helper, 2001)

        if payload[0] == 'voice':
            persistent = bool(payload[1])
            target = payload[2]
            raw_name = str(payload[3])
            audio_name = raw_name + '.mp3'
            data = ' '.join(payload[4:len(payload) - 1])

            # Check if the audio file already exists
            audio_files = os.listdir(self.audio_path)
            if audio_name in audio_files and payload[3] != str(None):
                content = str(payload[3]) + '.txt'

                # If it was a voice message it should have a content file, overwrite otherwise
                if content in audio_files:
                    # Check if the existing voice file has the same text
                    with open(self.audio_path + content) as content_file:
                        helper = content_file.read()
                        if helper == data:
                            self.request_play_audio_file(audio_name, target, persistent)
                            return  # No need to create a new audio file

            thread = Thread(target=self.create_new_audio, args=(target, persistent, raw_name, data))
            thread.start()

    def create_new_audio(self, target, persistent, name, data):
        audio_object = gTTS(text=data, lang='en')
        audio_object.save(self.audio_path + name + '.mp3')

        with open(self.audio_path + name + '.txt', 'w') as content_file:
            content_file.write(data)

        sleep(1)
        self.request_play_audio_file(name + '.mp3', target, persistent)

    def request_play_audio_file(self, src, channel, persist):
        src = 'audio/' + src
        helper = {'src': src, 'channel': channel, 'persist': persist, 'origin:': self.session_info.username}
        target = self.check_channel_exist(helper['channel'])
        target.announce(helper, 2001)
