from datetime import datetime


class MessageFormat(object):
    def __init__(self, session_info, channels):
        self.session_info = session_info
        self.channels = channels

    def handle_message(self, msg):
        param = msg.split(' ', 1)
        target = param[0]
        message = param[1]

        if target[0] == '@':
            return self.do_personal_message(target[1:], message)

        channel = self.find_channel(target)
        formatted_message = self.format_message(message, target)
        channel.announce(formatted_message)

        if not channel:
            helper = {'status': -1, 'info': 'unknown channel'}
        else:
            helper = {'status': 0, 'info': 'success'}

        print("Message to " + target + ": " + message)

        return self.session_info.protocol.send_message(7, helper)

    def format_message(self, message, target):
        user = self.session_info.username
        time = datetime.now().strftime('[%H:%M]')
        return target + ' ' + time + '\t' + user + ' : ' + message

    def find_channel(self, target):
        for channel in self.channels:
            if channel.name == target:
                return channel
        return None

    def do_personal_message(self, target, message):
        for find in self.session_info.communication_handler.client_list:
            if find.username == target:
                time = datetime.now().strftime('[%H:%M]')

                format_msg = '@' + self.session_info.username + ' ' + time + '\t' + self.session_info.username + ' : ' + message
                find.protocol.send_message(6, format_msg)

                format_msg = '@' + target + ' ' + time + '\t' + self.session_info.username + ' : ' + message
                return self.session_info.protocol.send_message(6, format_msg)
