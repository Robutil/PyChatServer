import json

from server.logic.command_format import CommandFormat
from server.logic.message_format import MessageFormat

from server.util.debug import Debug


class PacketHandler:
    def __init__(self, session_info, channels, debugger=None):
        if not session_info.protocol:
            raise Exception('PacketHandler', 'No valid protocol found in session information')
        self.protocol = session_info.protocol

        self.session_info = session_info
        self.channels = channels
        self.content = None

        if debugger:
            # Inherit parent debugger
            self.debugger = debugger
        else:
            # Do not debug
            self.debugger = Debug(False)

    def handle_packet(self, msg):
        """
        Extract packet id and content
        :param msg: tuple: (int: packet_id, string: content)
        :return: boolean: message handled correctly
        Content may not be none, but can be an empty string.
        """

        if msg[0] is not None:
            packet_id = msg[0]

            if msg[1] is not None:
                self.content = msg[1]

                dbg_msg = "Packet id: {}, payload: {}".format(packet_id, self.content)
                self.debugger.print(dbg_msg)

                if packet_id == 0:
                    return self.ping_packet()
                elif packet_id == 2:
                    return self.authenticate()
                elif packet_id == 4:
                    return self.command_packet()
                elif packet_id == 6:
                    return self.message_packet()
                else:
                    self.debugger.print("Unidentified packet")

        return False

    def ping_packet(self):
        """
        Currently, this is for debug purposes only.
        :return: Boolean: whether a packet was send back
        """

        # Print content instead of extraction (this packet is undefined!)
        if self.content is not None:
            # Packet is undefined, so content is not required
            self.debugger.print(self.content)
            if self.content == "ping":
                reply = "pong"
            else:
                reply = "ping"
        else:
            reply = ""

        # Send pong message back with same id (0)
        return self.protocol.send_message(0, reply)

    def authenticate(self):
        if self.content is None:
            self.session_info.status = self.session_info.communication_handler.STATUS_DISCONNECTED
            return None
        else:
            if self.username_available(self.content) and len(self.content) < 9:
                self.session_info.username = self.content
                self.session_info.authentication.authentication = self.session_info.authentication.trusted
                helper = {'authenticated': True, 'info': 'success'}
            else:
                helper = {'authenticated': False, 'info': 'username too long or unavailable'}
            return self.session_info.protocol.send_message(3, json.dumps(helper))

    def username_available(self, check):
        for client in self.session_info.communication_handler.client_list:
            if client.username is not None and client.username == check:
                return False
        return True

    def command_packet(self):
        return CommandFormat(self.session_info, self.channels).handle_command(self.content)

    def message_packet(self):
        return MessageFormat(self.session_info, self.channels).handle_message(self.content)
