class Error:
    _debug = True

    def __init__(self, debug=False):
        self._debug = debug

    def log(self, msg):
        # TODO: write this into persistent storage
        if self._debug:
            print(msg)
