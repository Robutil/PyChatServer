#!python3
from server.logic.packet_handler import PacketHandler
from server.models.channel import Channel
from server.network.communication_handler import CommunicationHandler
from server.util.debug import Debug

TCP_SOCKET_PORT = 52000
WEB_SOCKET_PORT = 53000
MAX_CLIENT_NUMBER = 5
SERVER_IP = 'localhost'

debugger = Debug(True, Debug.DEBUG)

channels = [Channel("global")]


def on_message(session_info, msg):
    """
    This function is called when a message from a client is received.
    :param session_info: container for protocol, sock type, etc.
    :param msg: received message (id, content), may be None if connection terminated
    :return: refreshed session info
    """

    if not msg:
        print("Client disconnected!")
        for client_channel in session_info.channels:
            for server_channel in channels:
                if server_channel.name == client_channel.name:
                    server_channel.remove_client(session_info)
        return None

    logic = PacketHandler(session_info, channels)
    logic.handle_packet(msg)
    del logic

    return session_info


debugger.print("start")

# Setup network layer
network_layer = CommunicationHandler(TCP_SOCKET_PORT, WEB_SOCKET_PORT,
                                     MAX_CLIENT_NUMBER, on_message, host=SERVER_IP)
network_layer.start_server()
