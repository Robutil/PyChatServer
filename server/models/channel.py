class Channel(object):
    def __init__(self, name, max_user=-1):
        self.name = name
        self.max_user = max_user
        self.client_list = []

    def add_client(self, client):
        self.client_list.append(client)

    def remove_client(self, client):
        index = 0
        for helper in self.client_list:
            if helper.username == client.username:
                self.client_list.pop(index)
            index += 1

    def get_client_names(self):
        user_list = []
        for client in self.client_list:
            if client is not None:
                user_list.append(client.username)
        return sorted(user_list, key=str.lower)

    def announce(self, msg, payload_id=6, exclude=None):
        # This has some issues...
        for client in self.client_list:
            if exclude and client.username == exclude:
                pass
            else:
                client.protocol.send_message(payload_id, msg)
