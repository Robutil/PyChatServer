class ClientInfo(dict):
    """
    This is a placeholder class to make use of dynamic dictionary elements.
    This class inherits from the Dict class.
    """
    pass
