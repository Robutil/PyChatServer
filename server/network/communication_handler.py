import _thread
import asyncio
import threading
from socket import *

import websockets

from server.models.authentication import Authentication
from server.models.client_info import ClientInfo
from server.network.protocol import Protocol
from server.util.debug import Debug


class CommunicationHandler:
    """
    The communication is responsible for setting up specified ports and handling
    multiple clients. It provides an abstraction layer for the controller.
    """

    # TODO: Create protocol for web sockets with tuple (address, port) instead of just address

    # Set true when verbose
    _debugger = Debug(True)
    STATUS_CONNECTED = 1
    STATUS_DISCONNECTED = -1
    _SOCKET_TYPE_TCP = "TCP_SOCK"
    _SOCKET_TYPE_WEB = "WEB_SOCK"

    def __init__(self, port, web_port, max_clients, f_handler, host='127.0.0.1'):
        """
        The communication is responsible for setting up specified ports and handling
        multiple clients. It provides an abstraction layer for the controller.

        :param port: TCP port to bind to. This is where TCP clients connect.
        :param web_port: Web port to bind to. This is where web socket clients connect.
        :param max_clients: Maximum number of TCP clients to except (spawns threads separately)
        :param f_handler: Function to call when a message from any (web or raw) client is received.
        f_handler should have a session_info parameter, which is also returned. This variable is provided
        on each message.
        :param host: address to bind to. Altering this may result in unspecified behaviour.
        """

        # Save vars
        self.port = port
        self.web_port = web_port
        self.host = host
        self.f_handler = f_handler

        # Create an empty list for keeping track of connected clients
        self.client_list = []

        # Setup TCP port
        self.server_socket = socket()
        self.server_socket.bind((host, port))
        self.server_socket.listen(max_clients)

    def setup_web_socket(self):
        """
        Initialises web socket port. This function must be called in a separate thread
        since it will stall due to async behaviour (required for web sockets).
        """

        # Web sockets require async, it automatically deals with multiple clients
        async_sub_thread_req_loop = asyncio.new_event_loop()
        asyncio.set_event_loop(async_sub_thread_req_loop)

        # Setup web_socket port
        web_socket = websockets.serve(self.web_client_handler, self.host, self.web_port)

        asyncio.get_event_loop().run_until_complete(web_socket)
        asyncio.get_event_loop().run_forever()

    async def web_client_handler(self, web_socket, path=None):
        """
        This function is called when a client connects via web sockets.
        :param web_socket: the web socket of the connected client
        :param path: request URI (not needed)
        """

        # This object stores all info required in a session
        session_info = ClientInfo()

        # Create class which deals with the protocol and buffering stuff.
        ch_protocol = Protocol(web_socket, web_socket.remote_address[0], self._SOCKET_TYPE_WEB)

        # Pass the buffers and such to the session info
        session_info.protocol = ch_protocol
        session_info.status = self.STATUS_CONNECTED
        session_info.socket_type = self._SOCKET_TYPE_WEB
        session_info.communication_handler = self
        session_info.username = None
        session_info.authentication = Authentication()
        session_info.channels = []

        # Add client to connected clients list
        self.client_list.append(session_info)

        # Keep session alive
        while True:
            try:
                if session_info.status is self.STATUS_DISCONNECTED:
                    raise websockets.exceptions.ConnectionClosed

                # Web sockets handle packet fragmentation, but it arrives as string
                msg = await web_socket.recv()

                # Convert string object to workable byteArray
                msg = bytearray(msg, "UTF-8")

                # First 4 bytes represents packet_id int. We do not receive a packet length int.
                packet_id = ch_protocol.extract_integer(msg)

                dbg_msg = "Found packet id: {}, and content: {}".format(packet_id, msg[4:])
                self._debugger.print(dbg_msg)

                # pa ss message to controller. Refresh session info accordingly
                session_info = self.f_handler(session_info, (packet_id, msg[4:].decode()))

            except websockets.exceptions.ConnectionClosed:
                session_info.status = self.STATUS_DISCONNECTED
                web_socket.close()
                self.f_handler(session_info, None)

                # Make our own comparable, otherwise the first object is always removed
                index = 0
                for item in self.client_list:
                    if item.protocol == session_info.protocol:
                        self.client_list.pop(index)
                    index += 1

                return  # Cleaning up should be handled in f_handler

    def client_handler(self, client_socket, address):
        """
        This function is to be used in a thread. It handles incoming messages.
        """

        # This object stores all info required in a session
        session_info = ClientInfo()

        # Create class which deals with the protocol and buffering stuff
        ch_protocol = Protocol(client_socket, address)

        # Pass the buffers and such to the session info
        session_info.protocol = ch_protocol
        session_info.status = self.STATUS_CONNECTED
        session_info.socket_type = self._SOCKET_TYPE_TCP
        session_info.communication_handler = self

        # Add client to connected clients list
        self.client_list.append(session_info)

        while True:
            self._debugger.print("Waiting for message")

            # First receive message to buffer (This already returns a full message if there is any!)
            msg = ch_protocol.receive_message()

            self._debugger.print("Got message")
            self._debugger.print(msg)

            if msg:
                # Pass message to function specified at init, update session info
                session_info = self.f_handler(session_info, msg)
                ch_protocol.remove_message()

            if not ch_protocol.is_active():
                # If the connection is terminated (either via error or disconnect) the connection
                # flag is false
                self._debugger.print("Client terminated connection")

                # Let the handler know that we disconnected
                session_info.status = self.STATUS_DISCONNECTED
                self.f_handler(session_info, None)

                # Make our own comparable, otherwise the first object is always removed
                index = 0
                for item in self.client_list:
                    if item.protocol == session_info.protocol:
                        self.client_list.pop(index)
                    index += 1

                ch_protocol.end()
                return

    def start_server(self):
        # Create separate thread for web sockets. Init continues there due to async.
        new_thread = threading.Thread(target=self.setup_web_socket)
        new_thread.start()

        while True:
            self._debugger.print("Waiting for incoming connections")

            # Accept incoming connections (this is a blocking call)
            client_socket, address = self.server_socket.accept()

            self._debugger.print("New connection")

            try:
                self._debugger.print("Starting thread")

                # Start handler for each client
                _thread.start_new_thread(self.client_handler, (client_socket, address))

                self._debugger.print("Thread created")

            except _thread.error as ex:
                # This will probably never happen, it's here to please pyLint.
                print(ex)
                break

    def end(self):
        self.server_socket.close()
