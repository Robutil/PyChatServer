import asyncio
import json
import struct


class Protocol:
    """
    This class is responsible for handling incoming and outgoing messages per client
    I'll write more documentation as I go along. Please be considerate.
    """

    _protocol_len = 4
    _packet_id_len = 4
    _buffer_size = 4096
    _connection_active = True

    def __init__(self, connection, address, socket_type="TCP_SOCK"):

        # We assume the connection is active, since this method is called after an accept
        # If the connection is no longer active, the receive will fail anyways

        # Protocol stuff
        self.buffer = bytearray()
        self.last_message_length = 0

        # Network stuff
        self.address = address
        self.socket = connection

        # Deal with multiple types of sockets
        if socket_type != "TCP_SOCK":
            self.web_sock = True
        else:
            self.web_sock = False

    @staticmethod
    def extract_integer(buffer, index=0):
        """
        Returns integer from buffer at specified index (defaults to zero)
        :param buffer: buffer to extract from (must be instance of bytes)
        :param index: (optional) start reading int from specified byte
        :return: integer
        """
        return struct.unpack('!i', buffer[index:index + 4])[0]

    def get_message(self):
        """
        This function unwraps a complete message from the requested buffer
        Returns None when there is no full message in the buffer. This is a
        non-blocking call.

        When a message is returned, it is still in the buffer. The message
        should then be removed with the remove_message() function.

        @:returns: tuple(int, string): payload id, full message -> or null if no message available
        """

        # We do not buffer web socket messages (yet) fixme?
        if self.web_sock:
            return None

        # Bytes 0 up to 4 (containing 0, excluding 4) represent the payload length
        if len(self.buffer) >= 4:

            # TODO: replace with function call
            packet_length = struct.unpack('!i', self.buffer[:4])[0]

            # TODO: replace with neat buffer function calls

            # Check for full packet
            if len(self.buffer) >= packet_length + self._protocol_len:
                # Extract packet id
                shorthand_formatting = self._protocol_len + self._packet_id_len
                packet_id = self.buffer[self._protocol_len: shorthand_formatting]

                # TODO: replace with function call
                packet_id = struct.unpack('!i', packet_id)[0]  # Network order integer

                # Extract message from buffer, but keep buffer intact
                ret = self.buffer[shorthand_formatting: shorthand_formatting + packet_length]

                # Update last message length so the message can be removed
                self.last_message_length = packet_length

                # Return the message
                return packet_id, ret.decode()

        # No (valid) message
        return None

    def remove_message(self):
        """"
        Removes last message from the buffer.
        :return: boolean: whether the function actually removed a message
        """

        # We do not buffer web socket messages (yet) fixme?
        if self.web_sock:
            return None

        if self.last_message_length >= 0:
            # TODO: some weird stuff going on here
            del self.buffer[:self.last_message_length + self._protocol_len + self._packet_id_len]
            self.last_message_length = -1
            return True
        return False

    def set_blocking(self, blocking):

        # Web sockets default to async
        if self.web_sock:
            return None

        self.socket.setblocking(blocking)

    def receive_message_web_socket(self):
        # TODO: this
        pass

    def receive_message(self):
        """
        This function receives data and appends it to the recv_buffer.
        Returns None when there is no full message. When there is a full
        message it will return the message.
        """

        # We do not buffer web socket messages (yet) fixme?
        if self.web_sock:
            return self.receive_message_web_socket()

        try:
            # Do socket receive
            msg = self.socket.recv(self._buffer_size)

        except TimeoutError:
            # This is an expected exception
            pass

        except Exception as ex:  # too broad
            print(ex)  # Log error, preferably at an error handler

            # Shut down gracefully
            self.end()

        else:
            # socket returned a message
            if len(msg) > 0:
                # Append message to existing buffer
                self.buffer.extend(msg)

                return self.get_message()

            else:
                # Client disconnected
                self.end()

        # No messages ready
        return None

    async def send_message_web_socket(self, packet_id, json_object):
        packet_id = struct.pack('!i', packet_id)
        full_packet = packet_id + json_object.encode()

        packet = bytearray()
        packet.extend(packet_id)
        packet.extend(json_object.encode("utf-8"))

        # Note that the decoding is latin-1, otherwise we cant send every byte
        await self.socket.send(packet.decode("latin-1"))
        return True

    def send_message(self, packet_id, json_object):
        """
        Send msg to target socket according to internal protocol.
        This function blocks until the message is send, or it fails to
        do so.
        @:return: boolean: whether a complete message was send
        """

        if isinstance(json_object, dict):
            json_object = json.dumps(json_object)

        if not isinstance(packet_id, int):
            return False

        # Web sockets require a different protocol, since they are different
        if self.web_sock:
            # Create async stuff yet again for web sockets
            async_sub_thread_req_loop = asyncio.new_event_loop()
            asyncio.set_event_loop(async_sub_thread_req_loop)

            # Wait for the send call to finish in main thread
            return asyncio.get_event_loop().run_until_complete(self.send_message_web_socket(packet_id, json_object))

        # Convert json to bytes
        json_object = json_object.encode()

        # Calculate packet length and set packet id
        packet_length = struct.pack('!i', (4 + len(json_object)))
        packet_id = struct.pack('!i', packet_id)

        # Create a full packet
        packet = bytearray()
        packet.extend(packet_length)
        packet.extend(packet_id)
        packet.extend(json_object)

        # Send the packet (use a loop here because full packet delivery in one send is not guaranteed)
        bytes_send = 0
        while bytes_send < len(packet):

            # Try sending
            chunk_bytes_send = self.socket.send(packet[bytes_send:])

            # Check for errors
            if chunk_bytes_send == 0:
                # TODO: log error
                return False

            # Updates bytes send
            bytes_send = bytes_send + chunk_bytes_send

        # Message send successful
        return True

    def is_active(self):
        # We do not buffer web socket messages (yet) fixme?
        if self.web_sock:
            # This method is untested for web sockets fixme
            return self.socket.open

        return self._connection_active

    def end(self):
        """Terminates connection gracefully"""
        self.socket.close()
        self._connection_active = False
