import threading

from bot.util.similarity import similar
from bot.werewolf.models.events.ww_game_event import WwGameEvent
from bot.werewolf.models.roles.role_citizen import RoleCitizen
from bot.werewolf.models.roles.role_factory import RoleFactory
from bot.werewolf.models.roles.role_werewolf import RoleWerewolf
from bot.werewolf.models.stages.stage_role import StageRole


class WwGameNew(object):
    def __init__(self, protocol):
        # Communication related
        self.protocol = protocol
        self.username = protocol.callback.username
        self.channel = protocol.callback.bot_channel

        # Game related
        self.players = []
        self.game_started = False
        self.active_stage = None
        self.role_factory = RoleFactory(self)
        self.roles = self.role_factory.create_role_list('vanilla')
        self.dead_players = []

        # Threading related
        self.thread = None
        self.mutex = threading.Lock()

    def find_player_by_name(self, name, use_similarity=False):
        for player in self.players:
            if player.name == name:
                return player
            elif use_similarity:
                if similar(player.name, name):
                    return player
        return None

    def handle_event(self, ww_game_event):
        if ww_game_event.msg_type == WwGameEvent.type_command:
            self.handle_command(ww_game_event.msg_obj)

        elif ww_game_event.msg_type == WwGameEvent.type_private:
            self.handle_private_message(ww_game_event.msg_obj)

        elif ww_game_event.msg_type == WwGameEvent.type_channel_message:
            self.handle_channel_message(ww_game_event.msg_obj)

        elif ww_game_event.msg_type == WwGameEvent.type_player_left:
            pass  # Panic

    def handle_command(self, data):
        print(data)
        if data['message'] == 'start' and not self.game_started:
            if data['channel'] == self.channel:
                self.game_started = True
                self.thread = threading.Thread(target=self.start, daemon=True)
                self.thread.start()

        if data['message'] == 'stop' and self.game_started:
            if data['channel'] == self.channel:
                self.game_started = False

                # Other command: maybe stage related? TODO

    def handle_private_message(self, data):
        first_word = data['message'].split(' ', 1)[0]

        # Check if it was a bot command
        if first_word == self.username or first_word[0] == '#':
            if first_word[0] == '#':
                data['message'] = str(data['message'][1:])
            else:
                data['message'] = str(data['message'].split(' ', 1)[1])
            self.handle_command(data)

        else:
            # Otherwise it must be stage specific
            self.active_stage.on_private_message(data)

    def handle_channel_message(self, data):
        self.active_stage.on_channel_message(data)

    def start(self):
        self.active_stage = StageRole(self)
        while True:

            print('Dead players:')
            for player in self.dead_players:
                print(player.name)

            print('Alive players')
            for player in self.players:
                if player.alive:
                    print(player.name)

            self.active_stage.activate()

            werewolves = 0
            citizens = 0
            for player in self.players:
                if player.alive:
                    if player.role.get_name() == RoleWerewolf.get_name():
                        werewolves += 1
                    elif player.role.get_name() == RoleCitizen.get_name():
                        citizens += 1

            if werewolves == 0:
                self.protocol.bot_send_channel_message(self.channel, 'Citizens won')
                return
            elif werewolves >= citizens:
                self.protocol.bot_send_channel_message(self.channel, 'Werewolves won')
                return

            self.active_stage = self.active_stage.get_next_stage()

            # Restart
            if self.active_stage is None:
                self.active_stage = StageRole(self).get_next_stage()
