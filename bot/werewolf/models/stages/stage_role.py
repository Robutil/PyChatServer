import random

from bot.werewolf.models.stages.global_stage import Stage
from bot.werewolf.models.stages.stage_will import StageWill


class StageRole(Stage):
    @staticmethod
    def get_name():
        return 'RoleDistribution'

    @staticmethod
    def requires_typing():
        return False

    def __init__(self, game_callback):
        super().__init__(game_callback, self.get_name(), 3)
        self.client_protocol = self.game_callback.protocol

    def on_start(self):
        # Let everyone know their roles have been distributed in private chat
        announce_message = 'Your roles have been distributed in private chats,'
        ' the game will start in {} seconds'.format(self.duration)
        self.client_protocol.bot_send_channel_message(self.channel, announce_message)

        audio_message = 'sample.mp3'
        self.client_protocol.bot_request_audio_file(self.channel, audio_message, True)

        self.distribute_roles()

    def distribute_roles(self):
        role_factory = self.game_callback.role_factory

        for player in self.players:
            helper = random.choice(self.game_callback.roles)
            self.game_callback.roles.remove(helper)

            role_factory.create_role(helper, player)

        for player in self.players:
            role_info = player.role.get_description()
            self.client_protocol.bot_send_private_message(player.name, role_info)

    def on_channel_message(self, data):
        pass

    def on_private_message(self, data):
        pass

    def get_next_stage(self):
        return StageWill(self.game_callback)
