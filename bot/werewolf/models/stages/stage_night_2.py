from bot.werewolf.models.stages.global_stage import Stage
from bot.werewolf.models.stages.stage_result import StageResult


class StageNightPartTwo(Stage):
    @staticmethod
    def requires_typing():
        return True

    @staticmethod
    def get_name():
        return 'NightSecondPart'

    def __init__(self, game_callback):
        super().__init__(game_callback, self.get_name(), 10)
        self.client_protocol = self.game_callback.protocol

    def get_next_stage(self):
        return StageResult(self.game_callback)

    def on_start(self):
        super().on_start()

        text_message = 'Night part two'
        self.client_protocol.bot_send_channel_message(self.channel, text_message)

        audio_message = 'The first few hours of the night have passed, now it is the turn for others to spring ' \
                        'into action.'
        self.client_protocol.bot_request_audio_message(self.channel, audio_message, name=self.get_name())

    def on_end(self):
        super().on_end()

        for player in self.game_callback.players:
            player.role.reset_typed()

    def on_private_message(self, data):
        player = self.game_callback.find_player_by_name(data['origin'])
        player.role.on_private_message(data)

    def on_channel_message(self, data):
        pass
