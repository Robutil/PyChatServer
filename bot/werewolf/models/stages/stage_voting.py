from bot.werewolf.models.stages.global_stage import Stage


class StageVoting(Stage):
    @staticmethod
    def get_name():
        return 'StageVoting'

    @staticmethod
    def requires_typing():
        return True

    def __init__(self, game_callback):
        self.stage_time_in_seconds = 10
        super().__init__(game_callback, self.get_name(), self.stage_time_in_seconds)
        self.client_protocol = self.game_callback.protocol

    def get_next_stage(self):
        return None

    def on_start(self):
        super().on_start()

        announce_message = 'Use the public channel to vote on someone you want to hang' \
            .format(self.stage_time_in_seconds)

        self.client_protocol.bot_send_channel_message(self.channel, announce_message)
        self.client_protocol.bot_request_audio_message(self.channel, announce_message, name=self.get_name())

    def on_end(self):
        dead_player = self.check_dead_player_by_votes()

        if dead_player is None:
            announce_message = 'There was an equal vote, nobody died'
        else:
            announce_message = dead_player.name + ' was put to the gallows'

        self.client_protocol.bot_send_channel_message(self.channel, announce_message)
        self.client_protocol.bot_request_audio_message(self.channel, announce_message, name=self.get_name())

        for player in self.game_callback.players:
            player.role.reset_typed()

        if dead_player is not None:
            # Store results in callback
            dead_player.alive = False
            self.game_callback.dead_players.append(dead_player)

            msg = dead_player.name + '\'s role was ' + dead_player.role.get_name() + ' and wanted ' \
                                                                                     'you to know that: ' + dead_player.will
            self.client_protocol.bot_send_channel_message(self.channel, msg)

        super().on_end()

    def on_private_message(self, data):
        pass

    def on_channel_message(self, data):
        player = self.game_callback.find_player_by_name(data['origin'])

        # Cannot vote more than once
        if player.typed:
            return

        target = self.game_callback.find_player_by_name(data['message'])

        # If target player does not exist, check if slightly misspelled
        if target is None:
            target = self.game_callback.find_player_by_name(data['message'], use_similarity=True)
            if target:
                msg = player.name + ', did you mean: ' + target.name + '? You voted for ' + target.name + '.'
                self.client_protocol.bot_send_channel_message(self.channel, msg)

        # Set vote
        if target is not None:
            target.votes += 1
            player.typed = True
        else:
            msg = data['message'] + ' is not a valid player name'
            self.client_protocol.bot_send_channel_message(self.channel, msg)
