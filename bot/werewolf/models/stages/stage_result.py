from bot.werewolf.models.stages.global_stage import Stage
from bot.werewolf.models.stages.stage_discussion_1 import StageDiscussionOne


class StageResult(Stage):
    @staticmethod
    def get_name():
        return 'NightResult'

    @staticmethod
    def requires_typing():
        return False

    def __init__(self, game_callback):
        super().__init__(game_callback, self.get_name(), 10)
        self.client_protocol = self.game_callback.protocol

    def on_start(self):
        dead_players = []

        # Check whether someone dies due to voting
        dead_player = self.check_dead_player_by_votes()
        if dead_player:
            dead_players.append(dead_player)

        # Check whether someone dies because of role related stuff
        for player in self.players:
            if player not in self.game_callback.dead_players and not player.alive:
                dead_players.append(dead_player)

        # Inform the players
        if len(dead_players) == 0:
            announce_message = 'As the morning arises, so does everyone'
        elif len(dead_players) == 1:
            announce_message = 'As the night passes, so does ' + dead_player.name
        else:
            helper = ', '.join(dead_players)
            announce_message = 'As the night passes so do ' + helper

        self.client_protocol.bot_send_channel_message(self.channel, announce_message)
        self.client_protocol.bot_request_audio_message(self.channel, announce_message, name=self.get_name())

        # Kill them all
        for player in dead_players:
            player.alive = False
            msg = player.name + '\'s role was ' + player.role.get_name() + ' and wanted you to know that: ' + player.will
            self.client_protocol.bot_send_channel_message(self.channel, msg)

        # Store results in callback
        self.game_callback.dead_players.extend(dead_players)

    def on_channel_message(self, data):
        pass

    def on_private_message(self, data):
        pass

    def on_end(self):
        super().on_end()

        # Reset votes
        for player in self.game_callback.players:
            player.role.reset_full()

    def get_next_stage(self):
        return StageDiscussionOne(self.game_callback)
