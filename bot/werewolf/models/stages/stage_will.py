from bot.werewolf.models.stages.global_stage import Stage
from bot.werewolf.models.stages.stage_night_1 import StageNightPartOne


class StageWill(Stage):
    @staticmethod
    def get_name():
        return 'Will'

    @staticmethod
    def requires_typing():
        return False

    def __init__(self, game_callback):
        super().__init__(game_callback, self.get_name(), 4)
        self.client_protocol = self.game_callback.protocol

    def on_start(self):
        super().on_start()

        announce_message = 'Send your will to @{} within {} seconds and I will publish it' \
                           ' upon your death'.format(self.username, self.duration)
        self.client_protocol.bot_send_channel_message(self.channel, announce_message)

    def get_next_stage(self):
        return StageNightPartOne(self.game_callback)

    def on_channel_message(self, data):
        pass

    def on_private_message(self, data):
        # Change player will
        player = self.game_callback.find_player_by_name(data['origin'])
        player.will = data['message']

        # Inform the player
        will_changed_message = 'You updated your will. Change your will at any time using #will [msg].'
        self.client_protocol.bot_send_private_message(player.name, will_changed_message)
