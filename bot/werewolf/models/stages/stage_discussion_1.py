from bot.werewolf.models.stages.global_stage import Stage
from bot.werewolf.models.stages.stage_siesta import StageSiesta


class StageDiscussionOne(Stage):
    def get_next_stage(self):
        return StageSiesta(self.game_callback)

    @staticmethod
    def requires_typing():
        return False

    def __init__(self, game_callback):
        self.stage_time_in_seconds = 10
        super().__init__(game_callback, self.get_name(), self.stage_time_in_seconds)
        self.client_protocol = self.game_callback.protocol

    def on_start(self):
        announce_message = 'Use the next {} seconds to discuss without electronic magic' \
            .format(self.stage_time_in_seconds)

        self.client_protocol.bot_send_channel_message(self.channel, announce_message)
        self.client_protocol.bot_request_audio_message(self.channel, announce_message, name=self.get_name())

    @staticmethod
    def get_name():
        return 'StageDiscussionOne'

    def on_channel_message(self, data):
        pass

    def on_private_message(self, data):
        pass
