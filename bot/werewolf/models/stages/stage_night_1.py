from bot.werewolf.models.stages.global_stage import Stage
from bot.werewolf.models.stages.stage_night_2 import StageNightPartTwo


class StageNightPartOne(Stage):
    @staticmethod
    def get_name():
        return 'NightFirstPart'

    @staticmethod
    def requires_typing():
        return True

    def __init__(self, game_callback):
        super().__init__(game_callback, self.get_name(), 10)
        self.client_protocol = self.game_callback.protocol

    def get_next_stage(self):
        return StageNightPartTwo(self.game_callback)

    def on_start(self):
        super().on_start()

        text_message = 'Night part one'
        self.client_protocol.bot_send_channel_message(self.channel, text_message)

        audio_message = 'As the sun goes down people return to their homes. However, in these dark times ' \
                        'it is not uncommon for werewolves to rise and slaughter others.'
        self.client_protocol.bot_request_audio_message(self.channel, audio_message, name=self.get_name())

    def on_end(self):
        super().on_end()

        # TODO: remove this
        self.check_dead_player_by_votes()

        for player in self.game_callback.players:
            player.role.reset_typed()

    def on_private_message(self, data):
        player = self.game_callback.find_player_by_name(data['origin'])
        player.role.on_private_message(data)

    def on_channel_message(self, data):
        pass
