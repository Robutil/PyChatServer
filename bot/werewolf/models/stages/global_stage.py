from time import sleep

from bot.werewolf.models.events.ww_game_event import WwGameEvent


class Stage(object):
    def __init__(self, game_callback, name, duration_in_seconds):
        self.game_callback = game_callback
        self.name = name
        self.duration = duration_in_seconds
        self.channel = game_callback.channel
        self.username = game_callback.username
        self.players = self.game_callback.players
        self.stall = 0

    def activate(self):
        self.on_start()
        self.on_sleep()
        self.on_end()

    def on_event(self, ww_game_event):
        if ww_game_event.msg_type == WwGameEvent.type_private:
            return self.on_private_message(ww_game_event.msg_obj)

        elif ww_game_event.msg_type == WwGameEvent.type_channel_message:
            return self.on_channel_message(ww_game_event.msg_obj)

    def on_sleep(self):
        sleep(self.duration)

        timer = -1
        while timer != 0:
            with self.game_callback.mutex:
                timer = self.stall
                self.stall = 0

            sleep(timer)

        if self.game_callback.active_stage.requires_typing():
            while any(not player.typed and player.alive for player in self.game_callback.players):
                msg = 'All players need to type'
                self.game_callback.protocol.bot_send_channel_message(self.channel, msg)
                sleep(3)

    def stall(self, time):
        with self.game_callback.mutex:
            self.stall += time

    def on_start(self):
        for player in self.game_callback.players:
            player.role.on_stage_start(self)

    def on_end(self):
        for player in self.game_callback.players:
            player.role.on_stage_end(self)

    def check_dead_player_by_votes(self):
        dead_player = None
        votes = 0  # Separate vote variable because dead_player starts as None

        for player in self.game_callback.players:
            if player.alive:
                print(player.name + ' ' + str(player.votes))

                # Check if there was an equal vote
                if player.votes == votes:
                    dead_player = None

                elif player.votes > votes:
                    dead_player = player
                    votes = player.votes

        return dead_player

    @staticmethod
    def get_name():
        raise NotImplementedError("Subclass must implement abstract method")

    @staticmethod
    def requires_typing():
        raise NotImplementedError("Subclass must implement abstract method")

    def on_private_message(self, data):
        raise NotImplementedError("Subclass must implement abstract method")

    def on_channel_message(self, data):
        raise NotImplementedError("Subclass must implement abstract method")

    def get_next_stage(self):
        raise NotImplementedError("Subclass must implement abstract method")
