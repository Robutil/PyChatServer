from bot.werewolf.models.roles.global_role import Role


class RoleCitizen(Role):
    @staticmethod
    def get_name():
        return 'citizen'

    def get_description(self):
        return 'You are a citizen, vote to kill werewolves during the day'
