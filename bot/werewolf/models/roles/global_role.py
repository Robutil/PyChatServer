from bot.util.random_words import RandomWords
from bot.util.similarity import similar
from bot.werewolf.models.stages.stage_voting import StageVoting


class Role(object):
    def __init__(self, player_callback, game_callback):
        self.player = player_callback
        self.game_callback = game_callback
        self.send_private_message = game_callback.protocol.bot_send_private_message

    def get_description(self):
        raise NotImplementedError("Subclass must implement abstract method")

    @staticmethod
    def get_name():
        raise NotImplementedError("Subclass must implement abstract method")

    def on_stage_end(self, stage):
        if stage.requires_typing():
            if stage.get_name() == StageVoting.get_name():
                pass
            else:
                self.reset_typed()

    def on_stage_start(self, stage):
        if self.player.alive:
            if stage.requires_typing():
                if stage.get_name() == StageVoting.get_name():
                    # Player is required to submit vote in channel chat
                    pass
                else:
                    # Player is required to type a message in private chat
                    self.type_random_message()

    def on_private_message(self, data):
        stage = self.game_callback.active_stage

        if stage.requires_typing():
            self.check_if_random_message(data)

    def reset_typed(self):
        """
        Reset whether a player has typed in this stage.
        :return: 
        """
        with self.game_callback.mutex:
            self.player.typed = False

    def reset_full(self):
        """
        Resets typing and votes
        """
        with self.game_callback.mutex:
            self.player.votes = 0
        self.reset_typed()

    def type_random_message(self):
        helper = RandomWords()
        self.player.random_word = helper.generate_word()

        msg = 'Please type the following word to continue: ' + self.player.random_word
        self.send_private_message(self.player.name, msg)

    def check_if_random_message(self, data):
        attempt = data['message']

        # Player typed random message exactly
        if attempt == self.player.random_word:
            with self.game_callback.mutex:
                self.player.typed = True
            self.send_private_message(self.player.name, 'You typed your message')

        # Player typed something resembling the random message
        elif similar(attempt, self.player.random_word):
            with self.game_callback.mutex:
                self.player.typed = True
            self.send_private_message(self.player.name, 'Close enough. You typed your message')

        # Player didn't type the required message
        else:
            self.send_private_message(self.player.name, 'Please check your spelling and retype')
