from bot.werewolf.models.roles.role_citizen import RoleCitizen
from bot.werewolf.models.roles.role_werewolf import RoleWerewolf


class RoleFactory(object):
    def __init__(self, game_callback):
        self.game_callback = game_callback

    def create_role_list(self, game_mode):
        if game_mode == 'vanilla':
            return self.create_role_list_temp()
        else:
            print('Role factory: unknown game mode')

    @staticmethod
    def create_role_list_temp(werewolves=1, citizens=6, witch=0):
        werewolf = RoleWerewolf.get_name()
        citizen = RoleCitizen.get_name()

        role_list = []
        for i in range(werewolves):
            role_list.append(werewolf)

        for i in range(citizens):
            role_list.append(citizen)

        return role_list

    def create_role(self, name, player):
        if name == RoleWerewolf.get_name():
            player.role = RoleWerewolf(player, self.game_callback)

        elif name == RoleCitizen.get_name():
            player.role = RoleCitizen(player, self.game_callback)

        else:
            print('Role factory: Unknown role')
