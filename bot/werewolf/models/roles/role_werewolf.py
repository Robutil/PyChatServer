from bot.werewolf.models.roles.global_role import Role
from bot.werewolf.models.stages.stage_night_1 import StageNightPartOne


class RoleWerewolf(Role):
    @staticmethod
    def get_name():
        return 'werewolf'

    def on_private_message(self, data):
        if self.player.alive:
            stage = self.game_callback.active_stage

            # Werewolves are active in the first part of the night
            if stage.get_name() == StageNightPartOne.get_name():
                self.on_private_message_stage_night(data)

            else:
                super().on_private_message(data)

    def on_private_message_stage_night(self, data):
        player = self.player  # Readability

        # Player can not change vote
        if player.typed:
            self.send_private_message(player.name, 'You have already voted for ' + player.random_word)
            return

        # Get voted player object
        target = data['message']
        target_player = self.game_callback.find_player_by_name(target)

        # No target player found, check if it was misspelled
        if target_player is None:
            target_player = self.game_callback.find_player_by_name(target, use_similarity=True)
            if target_player is not None:
                self.send_private_message(player.name, 'Did you mean: ' + target_player.name + '?')

        # Target player found
        if target_player:
            # Target player was already killed
            if not target_player.alive:
                self.send_private_message(player.name, 'That player has already passed away')

            # There was a valid target player
            else:
                self.send_private_message(player.name, 'You voted for ' + target_player.name)
                player.typed = True
                player.random_word = target_player.name
                target_player.votes += 1

                # Inform partner(s)
                for partner in self.game_callback.players:
                    if partner not in self.game_callback.dead_players and partner.role == 'werewolf':
                        if partner.name != player.name:
                            self.send_private_message(partner.name,
                                                      player.name + ' voted for ' + target_player.name)

        # Target player does not exist
        else:
            self.send_private_message(player.name, 'No player exists with that name')

    def on_stage_start(self, stage):
        if self.player.alive:
            if stage.get_name() == StageNightPartOne.get_name():
                msg = 'Use your werewolf powers to terminate someone, but who? Please type the name of the' \
                      ' player you would like to eliminate'
                self.send_private_message(self.player.name, msg)

            else:
                super().on_stage_start(stage)

    def on_stage_end(self, stage):
        self.reset_typed()

    def get_description(self):
        # If there two werewolves in the game, inform the werewolves
        players = self.game_callback.players
        for player in players:
            if player.name != self.player.name and player.role.get_name() == self.get_name():
                return 'You are a werewolf, your ally is {}. You and your ' \
                       'ally get to slaughter people each night'.format(player.name)

        # There is only one werewolf in the game
        return 'You are a werewolf, you get to slaughter people at night'
