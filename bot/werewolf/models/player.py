class Player(object):
    def __init__(self, name):
        self.name = name
        self.will = 'this player had no will to write a will'
        self.role = None
        self.alive = True
        self.voted_on = []
        self.votes = 0
        self.random_word = ''
        self.typed = False
