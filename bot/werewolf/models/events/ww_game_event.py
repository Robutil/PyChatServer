class WwGameEvent(object):
    type_command = 1
    type_private = 2
    type_player_left = 3
    type_channel_message = 4

    def __init__(self, msg_type, msg_obj):
        self.msg_type = msg_type
        self.msg_obj = msg_obj
