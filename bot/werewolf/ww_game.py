import random
import threading
import time

from bot.util.random_words import RandomWords
from bot.util.similarity import similar
from bot.werewolf.models.events.ww_game_event import WwGameEvent


# Known bugs:
# People with powers can stall stages indefinitely
# You already typed your message notification received on first message
# Role notification on gallows deaths
# Equal vote takes into account dead people -> votes per person aren't reset
# Stages require typing for dead people


class WwGame(object):
    def __init__(self, protocol):
        self.protocol = protocol
        self.username = protocol.callback.username
        self.channel = protocol.callback.bot_channel

        self.day = 0
        self.game_started = False
        self.players = []
        self.active_stage = None
        self.init_stages = ['roles', 'will', 'night_1', 'night_2', 'result']
        self.stages = ['discussion_1', 'siesta', 'discussion_2', 'voting_1', 'voting_2', 'night_1', 'night_2',
                       'result']
        self.roles = ['werewolf', 'werewolf', 'citizen', 'twin', 'twin', 'witch', 'hunter']

        self.dead_players = []

        self.thread = None
        self.mutex = threading.Lock()

    def find_player_by_name(self, name, use_similarity=False):
        for player in self.players:
            if player.name == name:
                return player
            elif use_similarity:
                if similar(player.name, name):
                    return player
        return None

    def handle_event(self, ww_game_event):
        if ww_game_event.msg_type == WwGameEvent.type_command:
            self.handle_command(ww_game_event.msg_obj)

        elif ww_game_event.msg_type == WwGameEvent.type_private:
            self.handle_private_message(ww_game_event.msg_obj)

        elif ww_game_event.msg_type == WwGameEvent.type_channel_message:
            if self.active_stage == 'voting_1':
                self.handle_channel_message_stage_voting_1(ww_game_event.msg_obj)

        elif ww_game_event.msg_type == WwGameEvent.type_player_left:
            pass  # Panic

    def handle_command(self, data):
        print(data)
        if data['message'] == 'start' and not self.game_started:
            if data['channel'] == self.channel:
                self.game_started = True
                self.thread = threading.Thread(target=self.start, daemon=True)
                self.thread.start()

        if data['message'] == 'stop' and self.game_started:
            if data['channel'] == self.channel:
                self.game_started = False

        elif data['message'] == 'will':
            if str(data['channel'][1:]) == data['origin']:
                player = self.find_player_by_name(data['origin'])
                self.protocol.bot_send_message(data['channel'], player.will)

    def handle_private_message(self, data):
        first_word = data['message'].split(' ', 1)[0]

        # Check if it was a bot command
        if first_word == self.username or first_word[0] == '#':
            if first_word[0] == '#':
                data['message'] = str(data['message'][1:])
            else:
                data['message'] = str(data['message'].split(' ', 1)[1])
            self.handle_command(data)

        # Otherwise it must be stage specific
        if self.active_stage == 'will':
            player = self.find_player_by_name(data['origin'])
            player.will = data['message']

        elif self.active_stage == 'night_1':
            self.handle_private_message_stage_night_1(data)

        elif self.active_stage == 'night_2':
            self.handle_private_message_stage_night_2(data)

        else:
            # If it is not a command or a stage specific message it will be ignored
            pass

    def handle_channel_message(self, data):
        if self.active_stage == 'voting_1':
            self.handle_channel_message_stage_voting_1(data)

    def handle_channel_message_stage_voting_1(self, data):
        print('Handling vote message: ' + str(data))
        player = self.find_player_by_name(data['origin'])

        print(player.name + ' and ' + str(player.typed) + ' and ' + str(player.alive))

        if not player.typed and player.alive:  # If the player hasn't already voted
            target = self.find_player_by_name(data['message'])
            if target is None:
                # Try to find it using similarity
                target = self.find_player_by_name(data['message'], use_similarity=True)
                if target is not None and target.alive:
                    msg = 'Did you mean {}? {} voted for {}'.format(target.name, player.name, target.name)
                    self.protocol.bot_send_message(self.channel, msg)

            if target is not None:  # TODO: and target.alive -> bots cant handle this
                with self.mutex:
                    player.typed = True
                player.voted_on.append(target.name)
                target.votes += 1

    def handle_private_message_stage_night_1(self, data):
        player = self.find_player_by_name(data['origin'])

        if player.role == 'werewolf':
            if player.typed:
                self.protocol.bot_send_message('@' + player.name, 'You have already voted for ' + player.random_word)
                return

            target = data['message']
            target_player = self.find_player_by_name(target)

            if target_player is None:
                target_player = self.find_player_by_name(target, use_similarity=True)
                if target_player is not None:
                    self.protocol.bot_send_message('@' + player.name, 'Did you mean: ' + target_player.name + '?')

            if target_player and target_player.alive:
                self.protocol.bot_send_message('@' + player.name, 'You voted for ' + target_player.name)
                player.typed = True
                player.random_word = target_player.name
                target_player.votes += 1

                # Inform partner(s)
                for partner in self.players:
                    if partner not in self.dead_players and partner.role == 'werewolf' and partner.name != player.name:
                        self.protocol.bot_send_message('@' + partner.name,
                                                       player.name + ' voted for ' + target_player.name)

        if player.role == 'slut':
            target = data['message']
            # TODO: this

        else:  # Player is required to type a random word
            attempt = data['message']
            if attempt == player.random_word:
                with self.mutex:
                    player.typed = True
                self.protocol.bot_send_message('@' + player.name, 'You typed your message')

            elif similar(attempt, player.random_word):
                with self.mutex:
                    player.typed = True
                self.protocol.bot_send_message('@' + player.name, 'Close enough. You typed your message')

            else:
                self.protocol.bot_send_message('@' + player.name, 'Please check your spelling and retype')

    def handle_private_message_stage_night_2(self, data):
        player = self.find_player_by_name(data['origin'])

        if player.role == 'witch':
            if player.typed:
                self.protocol.bot_send_message('@' + player.name,
                                               'You have already made your decision: ' + player.random_word)
                return

            player.random_word = data['message']
            fragments = data['message'].split()
            print(fragments)

            if fragments[0] == 'save':
                resurrect = self.check_death_by_votes()
                resurrect.alive = True
                with self.mutex:
                    player.typed = True

            elif fragments[0] == 'kill':
                target_player = self.find_player_by_name(fragments[1])

                if target_player is None:
                    self.protocol.bot_send_message('@' + player.name,
                                                   'That player does not exist: ' + fragments[1])
                else:
                    target_player.alive = False
                    self.protocol.bot_send_message('@' + player.name, 'You killed: ' + target_player.name)
                    with self.mutex:
                        player.typed = True

            elif similar(fragments[0], 'do-nothing'):
                self.protocol.bot_send_message('@' + player.name, 'You did nothing')
                with self.mutex:
                    player.typed = True

        else:  # Player is required to type a random word
            attempt = data['message']
            if attempt == player.random_word:
                with self.mutex:
                    player.typed = True
                self.protocol.bot_send_message('@' + player.name, 'You typed your message')

            elif self.similar(attempt, player.random_word):
                with self.mutex:
                    player.typed = True
                self.protocol.bot_send_message('@' + player.name, 'Close enough. You typed your message')

            else:
                self.protocol.bot_send_message('@' + player.name, 'Please check your spelling and retype')

    def start(self):
        # TODO: move following into while loop, use init flag
        self.stage_distribute_roles()
        self.stage_create_will()
        self.stage_night_1()
        self.stage_night_2()
        self.stage_result()

        self.active_stage = self.stages[0]

        while self.game_started:
            print("Current stage: " + str(self.active_stage))

            if self.active_stage == 'discussion_1':
                self.stage_discussion_1()

            elif self.active_stage == 'siesta':
                self.stage_siesta()

            elif self.active_stage == 'discussion_2':
                self.stage_discussion_2()

            elif self.active_stage == 'voting_1':
                self.stage_voting_1()

            elif self.active_stage == 'voting_2':
                self.stage_voting_2()

            elif self.active_stage == 'night_1':
                self.stage_night_1()

            elif self.active_stage == 'night_2':
                self.stage_night_2()

            elif self.active_stage == 'result':
                self.stage_result()

            self.increase_stage()

    def increase_stage(self):
        index = self.stages.index(self.active_stage)
        if (index + 1) == len(self.stages):
            self.active_stage = self.stages[0]
        else:
            self.active_stage = self.stages[index + 1]

        self.reset_typed()

    def stage_distribute_roles(self):
        self.active_stage = 'roles'

        role_timer_in_seconds = 5
        self.protocol.bot_send_message(self.channel, 'Your roles have been distributed in private chats,'
                                                     ' the game will start in {} seconds'.format(role_timer_in_seconds))

        self.protocol.bot_send_command(self.channel, 'audio publish audio/sample.mp3 True')
        time.sleep(2)

        for player in self.players:
            player.role = random.choice(self.roles)
            self.roles.remove(player.role)

        for player in self.players:
            role_info = self.get_role_description(player.role, player.name)
            self.protocol.bot_send_message('@' + player.name, role_info)

        time.sleep(role_timer_in_seconds - 2)

    def stage_create_will(self):
        self.active_stage = 'will'

        stage_time_in_seconds = 5
        stage_message = 'Send your will to @{} within {} seconds and I will publish it' \
                        ' upon your death'.format(self.username, stage_time_in_seconds)

        self.protocol.bot_send_message(self.channel, stage_message)
        time.sleep(stage_time_in_seconds)
        self.protocol.bot_send_message(self.channel, 'Will stage ended')

    def stage_night_1(self):
        self.active_stage = 'night_1'
        self.reset_votes()

        stage_time_in_seconds = 10
        stage_message = 'As the sun goes down people return to their homes. However, in these dark times' \
                        ', which lasts for {} seconds, it is not uncommon for werewolves to rise ' \
                        'and slaughter others.'.format(stage_time_in_seconds)
        self.protocol.bot_send_message(self.channel, stage_message)

        stage_message = 'Those who do not have any (role-related) activities during the night are required to ' \
                        'type a random word which you will receive in your private chat. The game will not progress ' \
                        'until everyone has typed their random word'
        self.protocol.bot_send_message(self.channel, stage_message)

        helper = RandomWords()
        for player in self.players:
            if player not in self.dead_players:
                player.typed = False
                msg = ''

                if player.role == 'werewolf':
                    msg = 'Use your werewolf powers to terminate someone, but who? Please type the name of the' \
                          ' player you would like to eliminate'

                elif player.role == 'slut':
                    msg = 'Use your beau- Use your skills to sneak into another\'s bed. Who is your victim? ' \
                          'Please type the name of the player'

                else:
                    player.random_word = helper.generate_word()
                    msg = 'Please type the following word to continue: ' + player.random_word

                self.protocol.bot_send_message('@' + player.name, msg)

        time.sleep(stage_time_in_seconds)
        self.wait_till_all_players_typed()

    def stage_night_2(self):
        self.active_stage = 'night_2'

        stage_time_in_seconds = 10
        stage_message = 'The first few hours of the night have passed, now it is the turn for others to spring ' \
                        'into action. This stage will last for {} seconds'.format(stage_time_in_seconds)
        self.protocol.bot_send_message(self.channel, stage_message)

        dead_player = self.check_death_by_votes()
        if dead_player is None:
            return  # Nobody dies this night
        else:
            dead_player.alive = False

        helper = RandomWords()
        for player in self.players:
            if player not in self.dead_players:
                player.typed = False
                msg = ''

                if player.role == 'witch':
                    msg = '{} died a horrible and painful death. Do you wish to resurrect? Type: save. If you wish ' \
                          'to exact revenge type: kill [player_name]. ' \
                          'To do nothing type: no-thing'.format(dead_player.name)

                else:
                    player.random_word = helper.generate_word()
                    msg = 'Please type the following word to continue: ' + player.random_word

                self.protocol.bot_send_message('@' + player.name, msg)

        time.sleep(stage_time_in_seconds)
        self.wait_till_all_players_typed()

    def stage_result(self):
        msg = None
        deaths = []
        for player in self.players:
            if not player.alive and player not in self.dead_players:
                if not msg:
                    msg = player.name
                else:
                    msg = msg + ' and ' + player.name
                deaths.append(player)
                self.dead_players.append(player)

        if not msg:
            msg = 'Nobody died in the night'
        else:
            msg = msg + ' died during the night'

        self.protocol.bot_send_message(self.channel, msg)

        for player in deaths:
            msg = '{}\'s role was: {}.<br>{} wanted you all to know that: {}'.format(player.name, player.role,
                                                                                     player.name, player.will)
            self.protocol.bot_send_message(self.channel, msg)

        werewolves = 0
        citizens = 0
        alive = []
        for player in self.players:
            if player.alive:
                alive.append(player.name)

                if player.role == 'werewolf':
                    werewolves += 1
                else:
                    citizens += 1

        if werewolves >= citizens:
            self.game_started = False
            msg = 'Werewolves won, congratulations!'

        elif werewolves == 0:
            msg = 'Citizens won, congratulations!'

        else:
            msg = 'The following players are still alive: {}'.format(str(alive))

        self.protocol.bot_send_message(self.channel, msg)

        self.reset_votes()

    def stage_discussion_1(self):
        self.stage_discussion_shared(6)

    def stage_siesta(self):
        stage_time_in_seconds = 6

        # Inform players the game has continued by playing a notification
        self.protocol.bot_send_command(self.channel, 'audio publish audio/sample.mp3 True')

        stage_message = 'Siesta stage. Use the following {} seconds to use the chat-server\'s abilities' \
                        ', maybe send a private messages to someone verbally out of reach'.format(stage_time_in_seconds)

        self.protocol.bot_send_message(self.channel, stage_message)
        time.sleep(stage_time_in_seconds)
        self.protocol.bot_send_message(self.channel, 'Siesta stage ended')

    def stage_discussion_2(self):
        self.stage_discussion_shared(6)

    def stage_discussion_shared(self, stage_time_in_seconds):
        stage_message = 'Use the next {} seconds to discuss. Stop looking at the screen,' \
                        ' this is a real life discussion!'.format(stage_time_in_seconds)
        self.protocol.bot_send_message(self.channel, stage_message)
        time.sleep(stage_time_in_seconds)
        self.protocol.bot_send_message(self.channel, 'Discussion stage ended')

    def stage_voting_1(self):
        stage_time_in_seconds = 15
        self.reset_votes()

        # Inform players the game has continued by playing a notification
        self.protocol.bot_send_command(self.channel, 'audio publish audio/sample.mp3 True')

        stage_message = 'The time has come to vote, use this channel. Just type the name of the player you want dead'
        self.protocol.bot_send_message(self.channel, stage_message)

        self.wait_till_all_players_typed(notification_delay=stage_time_in_seconds)
        self.protocol.bot_send_message(self.channel, 'Voting stage ended')

    def stage_voting_2(self):
        stage_time_in_seconds = 6

        dead_player = self.check_death_by_votes()
        if dead_player is None:
            msg = 'There was an equal vote, nobody died'
        else:
            dead_player.alive = False
            msg = '{} was put to the gallows and died grasping for air.' \
                  '{}\'s last words were {}'.format(dead_player.name, dead_player.name, dead_player.will)
        self.protocol.bot_send_message(self.channel, msg)

        # Wait so the players have some time to digest what happened
        time.sleep(stage_time_in_seconds)

    def get_role_description(self, role, user):
        if role == 'werewolf':
            for player in self.players:
                if player.name != user and player.role == 'werewolf':
                    return 'You are a werewolf, your ally is {}. You and your ' \
                           'ally get to slaughter people each night'.format(player.name)
            return 'You are a werewolf, you get to slaughter people at night'

        elif role == 'twin':
            for player in self.players:
                if player.name != user and player.role == 'twin':
                    return 'You are a twin, your counterpart is {}'.format(player.name)
            return 'You are a twin, but your counterpart has not spawned yet. Are you playing with enough players?'

        elif role == 'citizen':
            return 'You are a citizen, use your wit to kill the werewolf with your vote during the day'

        elif role == 'witch':
            return 'You are a witch, use your abilities to save or poison someone during the night'

        elif role == 'hunter':
            return 'You are a hunter, go out in a bang as you have the ability to kill someone when you meet your doom'

    def reset_votes(self):
        for player in self.players:
            # if player.alive: # TODO: bots cant handle this
            player.votes = 0

    def reset_typed(self):
        for player in self.players:
            # if player.alive: # TODO: bots cant handle this
            player.typed = False

    def check_death_by_votes(self):
        # Find out who died
        dead_player = None
        votes = -1
        for player in self.players:
            if player not in self.dead_players:
                if player.votes > votes:
                    votes = player.votes
                    dead_player = player

        # Check that there were no equal votes
        for player in self.players:
            if player not in self.dead_players:
                if player.votes == votes and player.name != dead_player.name:
                    return None

        return dead_player

    def wait_till_all_players_typed(self, notification_delay=0):
        counter = 0
        while self.game_started:
            flag = False
            with self.mutex:
                for player in self.players:
                    if player not in self.dead_players:
                        if not player.typed:
                            print(player.name + ' has not yet typed')
                            flag = True

            if flag:  # Obsolete if
                if counter >= notification_delay:
                    self.protocol.bot_send_message(self.channel, 'All players have to type before the game continues')
                else:
                    counter += 1
                time.sleep(1)
            else:
                break
