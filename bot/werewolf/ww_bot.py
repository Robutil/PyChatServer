import threading

from bot.werewolf.models.player import Player

from bot.util.client_communication_layer import ClientCommunicationLayer
from bot.werewolf.models.events.ww_game_event import WwGameEvent
from bot.werewolf.ww_game_new import WwGameNew

bot_name = 'ww_bot'
bot_channel = 'ww_ch'

mutex = threading.Lock()
game = None


def on_join_channel(channel_name):
    """
    When the bot joins a channel check for users who were already in said channel
    :param channel_name: the channel that was just joined
    """
    protocol.bot_request_channel_users(channel_name)


def on_channel_user_list(channel, users):
    if channel == bot_channel:
        for user in users:
            if user != bot_name:
                game.players.append(Player(user))


def on_user_joined_channel(channel_name, user):
    if channel_name == bot_channel:
        game.players.append(Player(user))


def on_user_left_channel(channel_name, user):
    if channel_name == bot_channel:
        index = 0
        for p in game.players:
            if p.name == user:
                game.players.pop(index)
                game.handle_event(WwGameEvent(WwGameEvent.type_player_left, user))
                break

            index += 1


def on_channel_message(container):
    print("Channel_message: " + container['channel'] + " " + container['message'])
    if container['channel'] == bot_channel:
        command = check_for_commands(container)

        if not command:
            game.handle_event(WwGameEvent(WwGameEvent.type_channel_message, container))


def on_personal_message(container):
    game.handle_event(WwGameEvent(WwGameEvent.type_private, container))


def check_for_commands(container):
    first_letter = container['message'][0]
    if first_letter == bot_name[0] or first_letter == '#':
        if first_letter == '#':
            container['message'] = str(container['message'][1:])
            game.handle_event(WwGameEvent(WwGameEvent.type_command, container))
            return True
        else:
            fragments = container['message'].split(' ')

            if fragments[0] == bot_name:
                container['message'] = ' '.join(fragments[1:])
                game.handle_event(WwGameEvent(WwGameEvent.type_command, container))
                return True
    else:
        return False


handler = ClientCommunicationLayer(bot_name, bot_channel)
protocol = handler.protocol

protocol.you_joined_channel = on_join_channel
protocol.on_user_joined_channel = on_user_joined_channel
protocol.on_user_left_channel = on_user_left_channel
protocol.on_channel_message = on_channel_message
protocol.on_personal_message = on_personal_message
protocol.on_channel_user_list = on_channel_user_list

game = WwGameNew(protocol)

handler.connect()
