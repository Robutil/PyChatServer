import threading
from random import randint

from bot.util.client_communication_layer import ClientCommunicationLayer

players_to_spawn = 6

wrappers = []


class Wrapper(object):
    def __init__(self, layer):
        self.layer = layer
        self.witch_counter = 0

    def start(self):
        self.layer.protocol.on_channel_message = self.handle_message
        self.layer.protocol.on_personal_message = self.handle_message
        thread = threading.Thread(target=self.layer.connect, daemon=True)
        thread.start()

    def get_random_player(self, exclude=None):
        random_player = 'bot_' + str(randint(0, players_to_spawn))
        if int(random_player[4]) == players_to_spawn:
            random_player = 'Robbin'

        if random_player == exclude and exclude is not None:
            return self.get_random_player(exclude)
        return random_player

    def handle_message(self, packet):
        if packet['channel'] == '@ww_bot':

            # Type the following word
            if 'type the following word' in packet['message']:
                helper = packet['message'].split()
                word = helper[len(helper) - 1]
                self.layer.protocol.bot_send_message('@ww_bot', word)

            if 'werewolf' in packet['message']:
                self.layer.protocol.bot_send_message('@ww_bot', self.get_random_player())

            if 'spelling' in packet['message']:
                # Something went wrong
                self.layer.protocol.bot_send_message('@ww_bot', self.get_random_player())

            if 'no-thing' in packet['message']:
                if self.witch_counter == 0:
                    self.layer.protocol.bot_send_message('@ww_bot', 'kill ' + self.get_random_player())
                    self.witch_counter += 1
                elif self.witch_counter == 1:
                    self.layer.protocol.bot_send_message('@ww_bot', 'save')
                    self.witch_counter += 1
                else:
                    self.layer.protocol.bot_send_message('@ww_bot', 'no-thing')

        elif packet['channel'] == 'ww_ch':
            if 'vote' in packet['message']:
                self.layer.protocol.bot_send_message('ww_ch', self.get_random_player())


for i in range(players_to_spawn):
    wrappers.append(Wrapper(ClientCommunicationLayer('bot_' + str(i), 'ww_ch')))
    wrappers[i].start()

input()
