from difflib import SequenceMatcher


def similar(a, b, threshold=0.6):
    return SequenceMatcher(None, a, b).ratio() > threshold
