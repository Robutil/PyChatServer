import asyncio

import websockets

from bot.util.client_protocol import ClientProtocol


class ClientCommunicationLayer:
    def __init__(self, username, bot_channel, host='localhost', port=53000):
        self.host = host
        self.port = port
        self.username = username
        self.bot_channel = bot_channel

        self.protocol = ClientProtocol(self)

    def connect(self):
        async_loop = asyncio.new_event_loop()
        asyncio.set_event_loop(async_loop)
        asyncio.get_event_loop().run_until_complete(self.listen())

    async def listen(self):
        address = 'ws://' + self.host + ':' + str(self.port)
        async with websockets.connect(address) as socket:
            self.protocol.socket = socket
            self.protocol.do_handshake()

            while True:
                try:
                    msg = await socket.recv()
                    self.protocol.handle_message(msg)

                except websockets.exceptions.ConnectionClosed:
                    socket.close()
                    self.protocol.connection_terminated()

    def send_packet(self, packet):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self._send_packet(packet))
        loop.close()

    async def _send_packet(self, packet):
        return await self.protocol.socket.send(packet.decode('latin-1'))
