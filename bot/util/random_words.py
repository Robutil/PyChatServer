import random


class RandomWords(object):
    """
    Adapted by Robbin Siepman from http://pythonfiddle.com/random-word-generator/
    """

    def __init__(self) -> None:
        self.vowels = list('aeiou')
        self.consonants = list('bcdfghjklmnpqrstvwxyz')

    def generate_word(self, k_min=2, k_max=4):
        word = ''
        syllables = k_min + int(random.random() * (k_max - k_min))
        for i in range(0, syllables):
            word += self.gen_syllable()

        return word.capitalize()

    def gen_syllable(self):
        ran = random.random()
        if ran < 0.333:
            return self.word_part('v') + self.word_part('c')
        if ran < 0.666:
            return self.word_part('c') + self.word_part('v')
        return self.word_part('c') + self.word_part('v') + self.word_part('c')

    def word_part(self, k_type):
        if k_type is 'c':
            return random.sample([ch for ch in self.consonants], 1)[0]
        if k_type is 'v':
            return random.sample(self.vowels, 1)[0]
