import json
import struct


class ClientProtocol(object):
    ID_USERNAME_AUTHENTICATION = 2
    ID_USERNAME_AUTHENTICATION_RESPONSE = 3

    ID_YOU_JOINED_CHANNEL = 1001
    ID_SHOW_CHANNEL_USER_RESPONSE = 1003
    ID_USER_JOINED_CHANNEL = 1005
    ID_USER_LEFT_CHANNEL = 1007

    ID_COMMAND = 4

    ID_MESSAGE = 6
    ID_MESSAGE_RESPONSE = 7

    def __init__(self, callback, receive_own_messages=False):
        self.receive_own_messages = receive_own_messages
        self.socket = None
        self.callback = callback
        self.error_info = ""

        self.you_joined_channel = None
        self.on_user_joined_channel = None
        self.on_user_left_channel = None
        self.on_channel_message = None
        self.on_personal_message = None
        self.on_channel_user_list = None

    def do_handshake(self):
        packet = self.create_packet(self.ID_USERNAME_AUTHENTICATION, self.callback.username)
        self.callback.send_packet(packet)

    def bot_join_channel(self):
        msg = 'channel join ' + self.callback.bot_channel
        packet = self.create_packet(self.ID_COMMAND, msg)
        self.callback.send_packet(packet)

    def bot_send_message(self, target, message):
        packet = self.create_packet(self.ID_MESSAGE, target + ' ' + message)
        self.callback.send_packet(packet)

    def bot_send_channel_message(self, target, message):
        return self.bot_send_message(target, message)

    def bot_send_private_message(self, target, message):
        return self.bot_send_message('@' + str(target), message)

    def bot_send_command(self, target, command):
        packet = self.create_packet(self.ID_COMMAND, command + ' ' + target)
        self.callback.send_packet(packet)

    def bot_request_audio_message(self, channel, text, name=None, persistent=False):
        self.bot_send_command(channel, 'audio voice ' + str(persistent) + ' ' + channel + ' ' + str(name) + ' ' + text)

    def bot_request_audio_file(self, channel, src, persistent=False):
        self.bot_send_command(channel, 'audio publish ' + str(src) + ' ' + str(persistent))

    def bot_request_channel_users(self, channel):
        packet = self.create_packet(self.ID_COMMAND, 'channel show' + ' ' + channel)
        self.callback.send_packet(packet)

    def handle_message(self, msg):
        packet_id = self.extract_integer(msg)
        msg = msg[4:]

        print("Found packet id: " + str(packet_id) + ", payload: " + msg)
        if packet_id == self.ID_USERNAME_AUTHENTICATION_RESPONSE:
            self.handle_username_authentication(msg)
            self.bot_join_channel()

        elif packet_id == self.ID_YOU_JOINED_CHANNEL:
            self.handle_you_joined_channel(msg)

        elif packet_id == self.ID_MESSAGE:
            self.handle_targeted_message(msg)

        elif packet_id == self.ID_USER_JOINED_CHANNEL:
            self.handle_user_joined_channel(msg)

        elif packet_id == self.ID_USER_LEFT_CHANNEL:
            self.handle_user_left_channel(msg)

        elif packet_id == self.ID_SHOW_CHANNEL_USER_RESPONSE:
            self.handle_show_channel_users_response(msg)

    def connection_terminated(self):
        pass

    @staticmethod
    def extract_integer(buffer, index=0):
        """
        Returns integer from buffer at specified index (defaults to zero)
        :param buffer: buffer to extract from (must be instance of bytes)
        :param index: (optional) start reading int from specified byte
        :return: integer
        """
        if isinstance(buffer, str):
            swap = bytearray()
            swap.extend(map(ord, buffer))
            buffer = swap
        return struct.unpack('!i', buffer[index:index + 4])[0]

    @staticmethod
    def create_packet(packet_id, payload):
        packet_id = struct.pack('!i', packet_id)

        packet = bytearray()
        packet.extend(packet_id)
        packet.extend(payload.encode())
        return packet

    @staticmethod
    def json_to_dict(payload):
        return json.loads(str(payload))

    def handle_username_authentication(self, payload):
        data = self.json_to_dict(payload)
        self.error_info = data['info']
        return data['authenticated']

    def handle_you_joined_channel(self, payload):
        data = self.json_to_dict(payload)

        if self.you_joined_channel is not None:
            self.you_joined_channel(data['channel'])

    def handle_targeted_message(self, payload):
        helper = payload.split()
        msg = {'channel': helper[0], 'timestamp': helper[1], 'origin': helper[2], 'message': ' '.join(helper[4:])}

        if msg['origin'] == self.callback.username and not self.receive_own_messages:
            return

        if msg['channel'][0] == '@':
            if self.on_personal_message is not None:
                self.on_personal_message(msg)
        elif self.on_channel_message is not None:
            self.on_channel_message(msg)

    def handle_user_joined_channel(self, payload):
        helper = payload.split(' ')
        msg_target = helper[0]
        user = helper[3]

        if self.on_user_joined_channel is not None:
            self.on_user_joined_channel(msg_target, user)

    def handle_user_left_channel(self, payload):
        helper = payload.split(' ')
        msg_target = helper[0]
        user = helper[3]

        if self.on_user_joined_channel is not None:
            self.on_user_left_channel(msg_target, user)

    def handle_show_channel_users_response(self, payload):
        if self.on_channel_user_list is not None:
            helper = json.loads(payload)
            self.on_channel_user_list(helper['channel'], helper['users'])
