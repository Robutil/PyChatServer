import random
import socket
import string
import sys

import struct

HOST = ''  # Symbolic name, meaning all available interfaces
PORT = 58081  # Arbitrary non-privileged port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print('Socket created')


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# Bind socket to local host and port
try:
    s.bind((HOST, PORT))
except socket.error as msg:
    print('Bind failed')
    sys.exit()

print('Socket bind complete')

# Start listening on socket
s.listen()
print('Socket now listening')

# now keep talking with the client
while 1:
    # wait to accept a connection - blocking call
    conn, addr = s.accept()
    print('Connected with ' + addr[0] + ':' + str(addr[1]))
    while 1:
        data = conn.recv(4)
        print("Got: " + str(data))

        ret = id_generator(random.randint(3, 6))
        packet = bytearray()
        packet.extend(struct.pack('B', len(ret)))
        packet.extend(bytearray(ret, 'ascii'))

        conn.send(packet)

s.close()
