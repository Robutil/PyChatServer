from bot.util.client_communication_layer import ClientCommunicationLayer

my_bot_channel = None


def on_join_channel(channel_name):
    protocol.bot_request_channel_users(channel_name)
    global my_bot_channel
    my_bot_channel = channel_name
    print("BOT: Joined channel: " + channel_name)

    import _thread
    _thread.start_new_thread(thread_spawn_messages_after_delay, ())


def on_user_joined_channel(channel_name, user):
    print("BOT: User: " + user + " joined channel: " + channel_name)


def on_user_left_channel(channel_name, user):
    print("BOT: User: " + user + " joined channel: " + channel_name)


def on_channel_message(channel_name, message):
    print("BOT: Received channel [" + channel_name + "] message: " + message)


def on_personal_message(origin, message):
    print("BOT: Received personal msg [" + origin + "] message: " + message)


def on_channel_user_list(channel, users):
    print("BOT: show " + channel + " users " + str(users))


def thread_spawn_messages_after_delay():
    import time
    for i in range(5):
        protocol.bot_send_message(my_bot_channel, 'Hi this is a thread call: ' + str(i))
        time.sleep(1)


handler = ClientCommunicationLayer('bot_name', 'bot_ch')
protocol = handler.protocol

protocol.you_joined_channel = on_join_channel
protocol.on_user_joined_channel = on_user_joined_channel
protocol.on_user_left_channel = on_user_left_channel
protocol.on_channel_message = on_channel_message
protocol.on_personal_message = on_personal_message
protocol.on_channel_user_list = on_channel_user_list

handler.connect()
