### Simple chatserver

Server side is written in Python. Messages are handled via a websocket connection, client side is all JavaScript.

![example_1](https://gitlab.com/Robutil/PyChatServer/raw/master/examples/pychat_1.png)

![example_2](https://gitlab.com/Robutil/PyChatServer/raw/master/examples/pychat_2.png)

![example_3](https://gitlab.com/Robutil/PyChatServer/raw/master/examples/pychat_3.png)

The chatserver supports multiple console commands and very easy access.