var hostname = "192.168.1.107";
var port = "58081";

console.log("Started");

var connect_string = "ws://" + hostname + ":" + port + "/";
var server_socket = new WebSocket(connect_string);

server_socket.onmessage = function (msg) {
    console.log(msg);
};

server_socket.onopen = function () {
    console.log("Opened socket");
};

server_socket.onerror = function (p1) {
    console.log("Error: ", p1);
};
