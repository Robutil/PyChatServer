/* === LOGIC === */

function get_channel_index(channel_name) {
    for (var i = 0; i < channel_names.length; i++) {
        if (channel_names[i] === channel_name.toString()) {
            return i;
        }
    }
    return -1;
}

function get_channel_log(channel_name) {
    var index = get_channel_index(channel_name);

    if (index >= 0) {
        return message_logs[index];

    } else return null;
}

function post_to_channel_log(channel_name, data) {
    var log = get_channel_log(channel_name);
    var msg = data + "<br>";

    if (log !== null) {
        var index = get_channel_index(channel_name);
        message_logs[index] = log + msg;

    } else {
        channel_names.push(channel_name.toString());
        message_logs.push(msg);
    }
}

function authenticate() {
    send_packet(2, username);
}

function join_default_channel() {
    //6 command packet
    send_packet(4, "channel default")
}

function get_channel_users() {
    send_packet(4, "channel show " + active_channel);
}

function transform_interface() {
    container.style.width = "1000px";
    input.style.width = "790px";

    channel_cont.className = "channels";
    output.className = "output";
    client_cont.className = "clients";

    handle_cont.className = "handle";
    handle_cont.innerHTML = username;

    //TODO: make the following a function
    input_ready_cont.onclick = client_message_input;
    input.onkeydown = function (e) {
        var temp = check_if_arrow_key_pressed(e);
        if (temp !== 0) {
            var current = get_channel_index(active_channel);
            if (temp === -1) {
                if (current !== 0) {
                    change_active_channel(channel_names[current - 1])
                }
            } else {
                if ((current + 1) !== channel_names.length) {
                    change_active_channel(channel_names[current + 1])
                }
            }
        }
        if (check_if_enter_pressed(e)) client_message_input();
    };
}

function add_channel_button(channel_name) {
    var channel_button = document.createElement("button");
    channel_button.innerHTML = channel_name;
    channel_button.id = "button_" + channel_name;
    channel_button.onclick = channel_button_clicked;
    channel_button.style.width = "auto";
    channel_cont.appendChild(channel_button);
}

function remove_channel_button(channel_name) {
    var channel_button_element = document.getElementById("button_" + channel_name.toString());
    channel_cont.removeChild(channel_button_element);
}

function client_message_input() {
    if (input.value.length > 0) {
        if (input.value.charAt(0) === '/') {
            if (input.value.length >= 2) {

                if (active_channel.toString().charAt(0) === '@') {
                    if (input.value.charAt(1) === 'l') {
                        //send_packet(6, active_channel + " " + "[Resets screen]");
                        var fake_channel_leave = {};
                        fake_channel_leave["channel"] = active_channel;
                        handle_left_channel(JSON.stringify(fake_channel_leave));
                        input.value = "";
                    }

                } else {
                    send_packet(4, input.value.substr(1) + " " + active_channel);
                    input.value = "";
                }
            }
        } else if (input.value.charAt(0) === '@' && input.value.length > 3) {
            send_packet(6, input.value);
            input.value = "";

        } else {
            var message = active_channel + " " + input.value;
            input.value = "";
            send_packet(6, message);
        }
    }
}

function channel_button_clicked(mouse_event) {
    console.log("Clicked: " + mouse_event.target.innerHTML);
    var channel_name = mouse_event.target.innerHTML.toString();

    change_active_channel(channel_name)
}

function change_active_channel(channel_name) {
    active_channel = channel_name;

    output.innerHTML = "";
    output.innerHTML = get_channel_log(channel_name);
    output.scrollTop = output.scrollHeight;
    get_channel_users();
}

function handle_authentication_response(payload) {
    var result = JSON.parse(payload);

    if (result.authenticated !== true) {
        append_to_html("Authentication failed, retrying");
        username = username + "_";
        if (username.length > 8) append_to_html("Cannot predict a new name");
        else authenticate();

    } else {
        append_to_html("Authentication successful, username: " + username);

        transform_interface();
        join_default_channel();
    }
}

function handle_joined_channel(payload) {
    var channel_name = JSON.parse(payload).channel;
    output.innerHTML = "";

    if (channel_name[0] === '@') var msg = "Joined private chat: " + channel_name;
    else var msg = "Joined channel: " + channel_name;

    append_to_html(msg);
    add_channel_button(channel_name);
    active_channel = channel_name;
    get_channel_users();

    post_to_channel_log(channel_name, msg);
}

function handle_left_channel(payload) {
    var channel_name = JSON.parse(payload).channel;
    output.innerHTML = "";

    remove_channel_button(channel_name);
    var index = get_channel_index(channel_name);
    channel_names.splice(index, 1);
    message_logs.splice(index, 1);

    if (active_channel.toString() === channel_name.toString()) {
        active_channel = channel_names[0].toString();
    }

    output.innerHTML = get_channel_log(active_channel);
    get_channel_users();
}

function handle_show_channel_users_response(payload) {
    console.log(JSON.parse(payload));
    var users = JSON.parse(payload).users;

    client_cont.innerHTML = "";

    //No channel users in private chat
    if (users === null) return;

    for (var i = 0; i < users.length; i++) {
        console.log(i + ": " + users[i].toString());
        client_cont.innerHTML += users[i].toString() + "<br>";
    }
}

function handle_play_audio(payload) {
    console.log("Audio: " + payload);
    var audioMeta = JSON.parse(payload);

    var persist = Boolean(audioMeta.persist);

    if (!persist) {
        var channelBroadcast = audioMeta.channel;
        if (channelBroadcast.toString() !== active_channel.toString()) {
            return
        }
    }

    if (document.hidden) {
        audio_notification();
    } else {
        audio_play(audioMeta.src)
    }
}

function handle_message(payload) {
    console.log(channel_names);
    console.log(message_logs);

    var channel_name = payload.split(" ", 1).toString();
    var message = payload.substr(payload.indexOf(" ") + 1);

    if (channel_name.charAt(0) === '@') {
        var index = get_channel_index(channel_name);
        console.log("Private message: " + index);
        if (index === -1) {
            var jsonData = {};
            jsonData["channel"] = channel_name;
            handle_joined_channel(JSON.stringify(jsonData));
            handle_message(payload);
            return;
        }
    }

    post_to_channel_log(channel_name, message);
    if (active_channel.toString() === channel_name) append_to_html(message)
}

function controller(packet_id, payload) {
    packet_id = parseInt(packet_id);

    if (packet_id === AUTHENTICATE_RESPONSE_PACKET) {
        handle_authentication_response(payload);

    } else if (packet_id === YOU_JOINED_CHANNEL_PACKET) {
        handle_joined_channel(payload);

    } else if (packet_id === USER_JOINED_CHANNEL_PACKET || packet_id === USER_LEFT_CHANNEL_PACKET) {
        handle_message(payload);
        get_channel_users();

    } else if (packet_id === YOU_LEFT_CHANNEL_PACKET) {
        handle_left_channel(payload);

    } else if (packet_id === SHOW_CHANNEL_USERS_RESPONSE) {
        handle_show_channel_users_response(payload);

    } else if (packet_id === CLIENT_MESSAGE) {
        handle_message(payload);

    } else if (packet_id === MESSAGE_SEND_STATUS_RESPONSE) {
        //Message return
        var result = JSON.parse(payload);
        console.log("Message send with status [" + result.status + "]: " + result.info);

    } else if (packet_id === SERVER_INFO_MESSAGE) {
        append_to_html("<pre>");
        handle_message(payload);
        append_to_html("</pre>");

    } else if (packet_id === PLAY_AUDIO_FILE) {
        handle_play_audio(payload);

    } else append_to_html(payload)
}

/* === MAIN === */

var hostname = "localhost";
var port = "53000";
var username = "";
var active_channel = "";

var channel_names = [];
var message_logs = [];

const AUTHENTICATE_RESPONSE_PACKET = 3;
const YOU_JOINED_CHANNEL_PACKET = 1001;
const USER_JOINED_CHANNEL_PACKET = 1005;
const USER_LEFT_CHANNEL_PACKET = 1007;
const SHOW_CHANNEL_USERS_RESPONSE = 1003;
const YOU_LEFT_CHANNEL_PACKET = 1009;
const YOU_JOINED_PRIVATE_CHAT = 1011;
const SERVER_INFO_MESSAGE = 11;
const CLIENT_MESSAGE = 6;
const MESSAGE_SEND_STATUS_RESPONSE = 7;
const PLAY_AUDIO_FILE = 2001;

console.log("Started");

var connect_string = "ws://" + hostname + ":" + port + "/";
var server_socket = new WebSocket(connect_string);

server_socket.onmessage = function (msg) {
    read_packet(msg.data)
};

//DOM stuff
var container = document.getElementById("container");
var input = document.getElementById("user_input");
var input_ready_cont = document.getElementById("input_ready");
var channel_cont = document.getElementById("channels");
var output = document.getElementById("output");
var client_cont = document.getElementById("clients");
var handle_cont = document.getElementById("handle");
var body = document.body,
    html = document.documentElement;

var start = Math.max(body.scrollHeight, body.offsetHeight,
    html.clientHeight, html.scrollHeight, html.offsetHeight);

input.onkeydown = function (e) {
    if (check_if_enter_pressed(e)) input_ready();
};
input_ready_cont.onclick = input_ready;

function input_ready() {
    if (input.value.length < 3 || input.value.length >= 9) {
        //Stub
    } else {
        username = input.value;
        input.value = "";
        authenticate();
    }
}

