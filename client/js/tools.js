function check_if_enter_pressed(event) {
    if (!event) event = window.event;
    var keyCode = event.keyCode || event.which;
    if (keyCode == '13') {
        return true;
    } else return false;
}

function check_if_arrow_key_pressed(event) {
    if (!event) event = window.event;
    var keyCode = event.keyCode || event.which;
    if (keyCode == '38') {
        //Up arrow
        return 1;
    } else if (keyCode == '40') {
        //Down arrow
        return -1;
    } else return 0;
}

function convert_integer_to_string_manual(conv) {
    var result = "";
    for (var i = 0; i < 4; i++) { //We use 32-bit integers
        //Use bitwise operators to create char representations and concat them
        result += String.fromCharCode(parseInt((conv >> (24 - 8 * i)), 10)); //Radix 10 for decimals
    }
    return result;
}

function convert_string_to_integer_manual(conv) {
    var result = 0;
    for (var i = 0; i < 4; i++) { //We use 32-bit integers
        //Use bitwise operators to create integer from raw byte values
        result += conv.charAt(i).charCodeAt(0) << (24 - 8 * i);
    }
    return result;
}

function create_packet(id, content) {
    return convert_integer_to_string_manual(id) + content;
}

function send_packet(id, content) {
    return server_socket.send(create_packet(id, content));
}

//TODO: more this elsewhere, this is more a logic function
function read_packet(packet) {
    var packet_id = convert_string_to_integer_manual(packet);
    var content = packet.slice(4);
    console.log("Found packet id: " + packet_id + ", with content: " + content);
    controller(packet_id, content)
}

function append_to_html(content) {
    output.innerHTML = output.innerHTML + content + "<br>";
    output.scrollTop = output.scrollHeight;
}